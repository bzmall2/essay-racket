
This is a place for sharing things to learn with Racket. 

I hope to re-work the exploratory scripts for teacher tasks and data visualizations. Re-working the scripts in a ~scribble/lp2~ document with minimalist ~scribble/examples~ should help me to maintain and expand competence with programming. Explaining the code while making it more compact and coherent may also help me discover a graded course for a /Beginning Racket/ book.  

Learning and teaching /General Education in a Free Society/ could be more coherent if Racket serves as a common thread from math and algebra to writing and essays, from basic level classes to to reports and reproducible research. I'm particularly interested in opportunities to consider media literacy while working with ~plot~ and ~slideshow~. Implementing information principles by Howard Wainer and Edward Tufte is good practice for today's global citizens and information consumers. Education with computers can be consistent with the classic essays we can read in /Towards Liberal Education/.

The /Racket/ language coming from the tradition of /Lisp/ and /Scheme/ is classic. Paul Graham writes of essays and /Lisp/ as classic, like math. The classics do not get stale. 

Training the attention span by coming to understand recursion is worthy enough. This sort of training, along with Reproducible Research, may be possible with other languages such as /Python/ for general purposes and /R/ for statistics. But my feeling is that they do not provide the consistency that can be maintained with /Racket/.  And the /lambda/ (λ) of Racket (Lisp and Scheme) provide us with a specific link, through a mention of the lambda calculus of Alonzo Church to the /Principia Mathematica/ of Alfred North Whitehead and Bertrand Russel. 

With Alfred North Whitehead we can get into /Science and Society/ and the seeming conflict between religion and science: from here I.A. Richards's /Poetries and Sciences/ leads us to /Practical Criticism/ and the Graded Direct Method for language learning and reading. With Bertrand Russel we can get into /On Education/ and even George Orwell's /1984/ and his classic /Politics and the English Language/ essay for writing. Bertrand Russel is also the only /intellectual/ Noam Chomsky respects. Russel's activity with the People's Tribunal on the Invasion of Vietnam leads us the tribunal on pollution: Bhopal and Minamata.

Minamata, provides a keyhole through which to see our earth more clearly. Data from classic Minamata books by Masazumi Harada will let us discover questions while creating views (visualizations) with Racket. Michiko Ishimure's work shows us the power of literature: the word. And we can work on coherent writing, grading the development of our thoughts from sentence to sentence on the model of cons lists. Good education with Racket should enable anyone to create their own textbook.  Racket lists and Scribble documentation will let us keep our means and ends appropriately matched. 

Writing Racket will help us to become "effectively capable" people: confident and competent citizens of our earth.
