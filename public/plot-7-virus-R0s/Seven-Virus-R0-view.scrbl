#lang scribble/manual
@; scribble/lp2 ;; too much, just work through examples

@; scribble --quiet ++style quiet-dep-broken-links.css Seven-Virus-R0-view.scrbl

@(require racket/list)
@(require plot plot/utils 
	 scribble/example
		(for-label racket/base))

@title[#:tag "plot-7-virus-R0"]{Plot 7 Virus Reproduction Numbers}

@(require scriblib/autobib)
@(define-cite ~cite citet generate-bibliography)
@(require racket/include)
@(include "../bibliography/sources.rkt")
@(require scriblib/footnote)


Let's learn @racket[racket]'s @racket[plot] with a simple table of reproduction numbers for seven well-know viruses. Learning a few @racket[plot] techniuques with simple data should help us keep in mind that data visualization is to help us get started asking questions. The important differences among these viruses is in how avoidable and treatable they are. We really want to know how to respond to threats as decent persons hoping to create, sustain, and live in a good society.

@(define 7v-eval (make-base-eval))
@(7v-eval '(require racket/list plot plot/utils
                    (for-label racket/base)))
@(define-syntax-rule (7v-example e ...)
  (examples  #:eval 7v-eval e ...))
@; #:no-inset #:no-prompt  

@7v-example[
(define data-table
  '((Covid-19 2 3.11)
    (Zika 3 6.6)
    (Measles 11 18)
    (Ebola 2 )
    (HIV 3.6 3.7)
    (Flu 1.3 )
    (Noro 1.6 3.7)))
(first data-table)
]

Each list in the table is the name of a viral disease with estimates of it's reproduction number. The reproduction numbers from an on-line news article's visualization@~cite[VoxCorona] that uses too much space and ink, in my opinion. The opinion was informed by reading Edward Tufte@~cite[TufteBeautifulEvidence] and Howard Wainer.@~cite[WainerOrderAdvice]

Howard Wainer advises us to ``order the rows and columns in a way that makes sense'' and that one useful way ``to order the data'' is by size. With @racket[sort] it is easy to order the data  by size so that the virus with the highest reproduction number will appear where viewers are most likely place their attention: ``Often we look most carefully at what is on top and less carefully further down.'' @~cite[(in-bib WainerOrderAdvice ", p. 19")]

@7v-example[
(define table-sort< (sort data-table #:key last <))
table-sort<
]

Using @racket[sort] with @racket[>] would give us lists in the order that Wainer recommennds. These @code{sorted lists} are in the opposite order, but when we plot the data the reverse order will work well.

For a comparison of the reproduction numbers I want to make lines for each virus. To see the difference between the minimum and maximum estimates I will just change the width of the lines. The line from zero to the minimum will be three times thicker than the line from the minimum to the maximum.

@7v-example[
(define (From0toXline x y)
  (lines (list (vector 0 y) (vector x y)) #:width 2.5))
(define (FromXtoxline x1 x2 y)
  (lines (list (vector x1 y) (vector x2 y)) #:width 1))
(plot
  (list
    (From0toXline 1.6 5)
    (FromXtoxline 1.6 3.7 5)))
]


It's a relief to see that the line-plotting procedures work when @racket[list]ed for @racket[plot]. We geta feeling of confidence that @racket[plot] draw everything in a list. With a few details we can implement guidelines by Solomon Messing are in the same spirit as advice we get from Edward Tufte and Howard Wainer: ``Remove as much chart junk as possible–unnecessary gridlines, shading, borders, etc.''@~cite[SolomonMessingBlog]

@7v-example[
(parameterize
  ((plot-decorations? #f)
   (plot-height 200))
 (plot
  (list
    (From0toXline 1.6 4)
    (FromXtoxline 1.6 3.7 4))
    #:x-min -.5  #:x-max 7
    #:y-min   3  #:y-max 5
    #:title "Reproduction numbers for seven diseases"))
]

Now we have confidence, not only that plot will draw everything in a list, but also that we can control "chart junk" and plot size. Now let's put together a procedure to help visualize one viral disease.


@7v-example[
(require racket/string)
(code:comment "for string-join string-append ..." )

(define R0-min-max-sep " - ")
(define (label-disease-R0 disease-row y)
  (define label-strings (map ->plot-label disease-row))
  (define label-string (string-append (first label-strings) ":  "
                (string-join (rest label-strings) R0-min-max-sep)))
  (point-label (vector 0 y) label-string #:point-sym 'none))

(define line-offset .3)
(define (0toXtoxline x1 x2 y)
  (list (From0toXline  x1 y) (FromXtoxline x1 x2 y)))

(define (plot-a-R0 disease-row y)
  (define line-y (- y line-offset))
  (if (= 2 (length disease-row))
     (list
      (From0toXline (second disease-row) line-y)
      (label-disease-R0 disease-row y))
     (list
      (0toXtoxline (second disease-row) (third disease-row) line-y)
      (label-disease-R0 disease-row y))))


(parameterize
  ((plot-decorations? #f)
   (plot-height 100))
   (plot
     (plot-a-R0 (list-ref table-sort< 4) 4)
    #:x-min -.5  #:x-max 7
    #:y-min   3  #:y-max 5
    #:title "Reproduction numbers for seven diseases"))

]

If we can plot one disease we can plot them all. It is just @code{List Porcessing} and @code{racket} emerged from @code{Scheme} and  @code{Lisp} (@code{Lis}t @code{p}rocessing): the inspiration started with Alonzo Church @code{λ}@code{labmda} calculus. Church's reasons for choosing @code{λ} leads us to mention Alfred North Whitehead and Bertrand Russel. Beginning programming gives us a graded entry @italic{Towards Liberal Arts}, into the Humanties from digital screens.


Traditionally @racket[map] is used to generate a list from a list. Since we need to keep track of the number of times we process a disease list this exercise is a candidate for practice with @italic{recursion}, even @italic{tail recursion} with @italic{auxiliary} or @italic{helper} functions. But, it seems more compact to work with a @italic{List Comprehension} for this short introductory example.

@7v-example[
(define Title-1 "Reproduction numbers for various diseases: ")
(define Title-2 "... one sick person is like to infect how many?")
(define source-url-a "https://www.vox.com/2020/2/18/21142009/")
(define source-url-b "             coronavirus-covid19-china-wuhan-deaths-pandemic")

(define (plot-viral-diseases sorted-lists)
  (define x-max (add1 (last (argmax last sorted-lists))))
  (define y-max (add1 (length sorted-lists)))
  (parameterize
    ((plot-decorations? #f)
     )
   (plot	  
     (list

      (for/list ([d-list sorted-lists]
                 [y (in-naturals)])
         (plot-a-R0 d-list y))

      (point-label (vector 0 (- y-max .3)) Title-1 #:point-sym 'none)
      (point-label (vector 0 (- y-max .8)) Title-2 #:point-sym 'none)
      (point-label (vector 0 (- y-max 1.3)) source-url-a #:point-sym 'none #:size 8)
      (point-label (vector 0 (- y-max 1.6)) source-url-b #:point-sym 'none #:size 8))
    #:x-min -.5  #:x-max x-max
    #:y-min -.5  #:y-max y-max)))

(plot-viral-diseases table-sort< )

]


@(require scriblib/footnote)

I'm wondering if this sort of extended but simple example could help learners gain confidence while useing @racket[plot]. For a GDM@note{GDM: @italic{The Graded Direct Method} developed by I.A. Richards and Christine Gibson for learners of English as a new language}-style lesson the examples could be more finely graded. If there is every a chance to go through something like this in a classroom I'll have to look into a breaking the sequence into smaller steps with @racket[slideshow].

@;{
For the iterative development of visualizations it helps to start working with the plots in DrRacket.
@bz2-image[screenshot]
}

@(generate-bibliography)


