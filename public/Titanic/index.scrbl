#lang scribble/manual

@(require scribble/example
	    (for-label racket))

@(require racket/list plot plot/utils racket/include)
@(include "utils.rkt")

@title[#:tag "titanic-categorical-variables" #:style '(no-toc no-sidebar)]{
Titanic: Summarising Categorical variables with Racket}

@; 2020年  3月 27日 金曜日 21:59:57 JST
@; https://www.sheffield.ac.uk/polopoly_fs/1.714591!/file/stcp-karadimitriou-categoricalR.pdf
@; the statstutor resource has data for 1309 passengers, what about crew?
@; the data I found on github has only 891 and some persons worth of data
@; maybe it's missing the crew??

You can't be neutral on the speeing @italic{Titanic}. The sentence is adapted from Howard Zinn's line: ``You can't be neutral on a moving traing.'' I always see a steam engined, fossil-fuel powered, train rolling along tracks that hang out over an abyss. 

So, how can we summarise the data, this table of values. We can think of a table as columns and rows. @italic{Racket} emerged from the language named for @code{lis}t @code{p}rocessing: @italic{Lisp}. When working with @italic{Racket} it's easy to think of a table as a list of lists.
@; Panir moto shohoj: Easy like water, that's Bengali for a Taoist thought I imagine.

It's hard to remember the little details and syntax of @italic{R}. If we get into the habit or programming repetitive tasks with @italic{Racket} we can think our way through any data-driven task as well. Our work with computers will be coherent. With coherency, "an intellible structure to explore" (I.A. Richcards) we are much more likely to become effectively capable persons.

With @racket[racket] We are free to summarize the data with functions that are of general use. We can learn statistics while also learning to make use of procedures that will help us to work in other fields.
@; write programs for anything the web, blogging, slideshows, web or gui applications...
@; ZuKan synopis


@(define titanic-eval (make-base-eval))
@(titanic-eval
 '(require racket/list csv-reading racket/string plot plot/utils
    (for-label racket/base)))
@(define-syntax-rule (titanic-ex e ...)  (examples #:label
"Code:" #:eval titanic-eval e ...))

@(define titan-data "titanic.csv")

@titanic-ex[
(define titan-data "titanic.csv")
(define (get-file-csv-data file-name)
  (define f-port (open-input-file file-name))
  (define data-lists (csv->list f-port))
  (close-input-port f-port)
  (code:comment "close-input-port can't be last line orthe procedure returns void")
  data-lists)
  
(define titanic-lists
  (get-file-csv-data titan-data))
(code:comment "(take titanic-lists 3)")
]

@(define titanic-lists
  (get-file-csv-data titan-data))

@; why isn't row-properties working??
@(tabular #:sep (hspace 1)
          #:row-properties '(border-bottom ())
          (take titanic-lists 3))

There were 1309 people on the Titanic according to the @italic{statstutor} page, but as I start this attempt I only have data for 891 people. It was before cruise ships got associated with death by virus. The people were segregated into three classes: first, second, and third are represented by @code{1}, @code{2}, and @code{3} in the column headed by @code{Pclass}. How many people of each class are represented in the data?

Let's number the headers so we can use @racket[list-ref] to answer questions about the data.

@titanic-ex[
(define (number-list-elements lst)
  (for/list ((elem lst)
               (num (in-naturals)))
    (list num elem)))

(number-list-elements (first titanic-lists))
]


It looks like the tenth column of the data table is headed with @code{Fare}. I wonder what the lowest and highest fares were? Who paid the most for the ticket on the Titanic? Let's take a look at the @code{Fares}.

@titanic-ex[
(define Fares
  (map (lambda (row) (list-ref row 9))
       titanic-lists))

(take Fares 5)
]


The @code{Fares} are strings with periods (decimal places) in them. The procedures like @racket[max] will @racket[blame] our attempts to use them unless we convert the strings in @racket[number]s.

@titanic-ex[
(define (titanic-string->number t-str)
  (string->number
   (string-replace t-str "." "")))

(define (list-slice from to lst)   ;; inclusive
   (drop (take lst to) (sub1 from)))

(cons (first Fares) (map titanic-string->number
                        (list-slice 2 5 Fares)))
]

Now that @racket[racket] can work with numbers we can use @racket[argmax] to find who spent the most to get on the @italic{Titanic}.

@titanic-ex[
(argmax (lambda (row)
           (titanic-string->number
            (list-ref row (sub1 10))))
        (rest titanic-lists))
]	

@tabular[#:sep (hspace 1)
         #:row-properties '(border-bottom ())
        (cons (first titanic-lists)
	(list 
	(argmax (lambda (row)
           (titanic-string->number
            (list-ref row (sub1 10))))
        (rest titanic-lists))))
]

For the Titanic data about 891 person that I am walking with to make this path, Miss Anna Ward, age 35, paid 512,3292@bold{!?!} for her ticket and survived. The data seems off, but I can just replace the data file and have @racket[scribble] generate the document with a better data set. Maybe someone represented in the 1309-line data set paid more than Miss Anna Ward. If so, I wonder if they died.

What will we see after replacing @racket[argmax] with @racket[argmin]?

@titanic-ex[
(argmin (lambda (row)
           (titanic-string->number
            (list-ref row (sub1 10))))
        (rest titanic-lists))
]	

Mr. Lionel Leonard seems to have paid no money to get on the Titanic, and he died. Douglas Rushkoff, writing about the internet and tech says that nothing is more costly than ``@italic{free}'' services.

@tabular[#:sep (hspace 1)
         #:row-properties '(border-bottom ())
        (cons (first titanic-lists)
	(list 
	(argmin (lambda (row)
           (titanic-string->number
            (list-ref row (sub1 10))))
        (rest titanic-lists))))
]

Maybe Mr. Lionel Leonard worked for his fare or something. I didn't watch the movie.

I wonder how many people paid the "0" @code{Fare}?

@titanic-ex[
      (length
	(filter (λ (row) (string=? "0"
	                   (list-ref row 9)))
	  (rest titanic-lists)))
]	

We could print all @code{15} lists but the table is unwieldy and we are exploring how to do @code{R}-like things with @code{racket}.

@;{ @; COMMENTED OUT
@tabular[#:sep (hspace 1)
        (cons (first titanic-lists)
	(filter (λ (row) (string=? "0"
	                   (list-ref row 9)))
	  (rest titanic-lists)))
] @; COMMENTED OUT
}

The table is unwieldy. To practice summarizing categorical variables as in the @italic{statsutor} pages, we only need the second and third columns. 

@titanic-ex[
(define survi-class
  (map (λ (row) (drop (take row 3) 1))
    titanic-lists))
(take survi-class 5)
]


@(define survi-class
  (map (λ (row) (drop (take row 3) 1))
    titanic-lists))
    
@tabular[#:sep (hspace 1) #:row-properties '(border-bottom ())
         (take survi-class 5)]


@titanic-ex[
(define t-died  (count (λ (row) (string=? "0" (first row)))
                 survi-class))
(define t-lived (count (λ (row) (string=? "1" (first row)))
                 survi-class))

(list '("Died" "Lived" "All")
      (list t-died t-lived (+ t-died t-lived)))
]

@(define t-died  (count (λ (row) (string=? "0" (first row)))
                 survi-class))
@(define t-lived (count (λ (row) (string=? "1" (first row)))
                 survi-class))

@(tabular #:sep (hspace 1)
          #:row-properties '(bottom-border ())
      (list '("Died" "Lived"  "All")
             (map number->string
	       (list t-died t-lived (+ t-died t-lived)))))

What are the proportions of the fregency table?

@titanic-ex[
(define All (+ t-died t-lived))
(define p-died (/ t-died All))
(define p-lived (/ t-lived All))

(list '("Died" "Lived")
       (list p-died p-lived))

(list '("Died" "Lived")
       (map (λ (s) (real->plot-label s 2))
         (list p-died p-lived)))

(list '("Died" "Lived")
       (map (λ (n) (string-append
                      (real->plot-label (* 100 n) 0) "%"))
         (list p-died p-lived)))
]

@(define All (+ t-died t-lived))
@(define p-died (/ t-died All))
@(define p-lived (/ t-lived All))


@(tabular #:sep (hspace 1)
          #:row-properties '(bottom-border ())
       (list '("Died" "Lived")	  
         (map (λ (n) (string-append
                      (real->plot-label (* 100 n) 0) "%"))
         (list p-died p-lived))))


How many people were grouped into each class: @code{C3}, @code{C2}, and @code{C3}.

@titanic-ex[
(define C3 (count (λ (c) (equal? "3" c))
             (map second survi-class)))
C3	     
(define C2 (count (λ (c) (equal? "2" c))
             (map second survi-class)))
C2	     

(define C1 (count (λ (c) (equal? "1" c))
             (map second survi-class)))
C1	     
]

@(define C3 (count (λ (c) (equal? "3" c))
             (map second survi-class)))
@(define C2 (count (λ (c) (equal? "2" c))
             (map second survi-class)))
@(define C1 (count (λ (c) (equal? "1" c))
             (map second survi-class)))

@(tabular #:sep (hspace 1)
          #:row-properties '(bottom-border ())
      (list '("C3" "C2"  "C1" "All")
             (map number->string
	       (list C3 C2 C1 (+ C3 C2 C1)))))


@;{
titanic-ex[
(code:comment "Convention: capital letter names mean column with header.")
(define Fares (map (λ (row) (list-ref row (sub1 10)))
                titanic-lists))

(define (col-top rows)
(code:comment "Like @italic{R}'s or bash's @code{top}")
  (take rows 5))
;; (col-top Fares)

(code:comment "skip header with `rest', convert strings for `sort'")
(take
  (sort (map string->number
    (map (λ (s) (string-replace s "." ""))
         (rest Fares)))
    <) 5)
(code:comment "will need numbers frequently so: ")

(define (get-titanic-numbers lst)
  (map string->number lst))

(define (col-top-head-w-nums rows)
  (define top (col-top rows))
  (cons (first top)
        (map get-titanic-numbers (rest top))))

(col-top-head-w-nums Fares)
]
}
