@facfreq-example[
(define (label-plot info-dict x y)
  (point-label (vector x y) label-base  #:anchor 'top-right #:size 10 #:point-sym 'none))

(define posi-color 'red)
(define (positives-line 4n 5n y)
  (list 
    (FromXtoXline 4n 5n y #:width 3 #:color posi-color)
    (From0toXline 4n y #:color posi-color)
    ))

(define (plot-a-question qlist y)
  (define opinion-y (- y opinion-line-offset))
  (define neutral-y (- y neutral-line-offset))


  (define short-name (first qlist))
  (define 4t (list-ref qlist 4)) ;; Text string
  (define 4n (string->number 4t)) ;; Number integer
  (define 5t (list-ref qlist 5)) (define 5n (string->number 5t))
  (define 2t (list-ref qlist 2)) (define 2n (string->number 2t))
  (define 1t (list-ref qlist 1)) (define 1n (string->number 1t))
  (define 3t (list-ref qlist 3)) (define 3n (string->number 3t))
  (define 3nmid (/ 3n 2))   
  (define q-label (get-q-text question-data-lists short-name))
  
  
  (list (point-label (vector -5 y) q-label #:point-sym 'none)
        (positives-line 4n 5n opinion-y)
	(From0toXline (- 2n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 2n) (- 1n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 3nmid) 3nmid neutral-y #:color 'gray)
   )
  )

(define (plot-questions-view data-lists)
  (define table-length (length data-lists))
  (define plot-h (* height-for-a-question table-length))
  (define x-max (add1 (get-opinioned-max data-lists 4 5)))
  (define x-min (sub1 (- (get-opinioned-max data-lists 1 2))))
  (define y-max (add1 table-length))
                       
  (parameterize
    ((plot-decorations? #f)
     (plot-height plot-h)
     (plot-width  plot-w))
    (plot
      (list
        (for/list ((level-frequency-list data-lists)
                   (y (in-naturals)))
          (plot-a-question level-frequency-list (add1 y)))
	  ;;(label-plot freq-info x-max y-max)
	  )
      #:x-min x-min #:x-max x-max
      #:y-min 0 #:y-max y-max)))

(plot-questions-view freq-lists)
]
