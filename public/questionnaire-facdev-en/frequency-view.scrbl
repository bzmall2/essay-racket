#lang scribble/manual

@; Hyperlinks distract readers from following a narrative:
@; scribble --quiet ++style "../quiet-dep-broken-links.css" frequency-view.scrbl
@; but  eventually the `for-label settings should be worked out.

@; emacs convenience:
@; (set-register ?] "@facfreq-example[ ")

@(require racket/list racket/include)
@(include "utils.rkt")

@(require plot plot/utils racket/string 
	 scribble/example
		(for-label racket/base))
@title[#:tag "facdev-questionnaire-frequency-view-en"]{Plot the Frequencies for Responses on Questionnaire Forms}

We have seen how to take @racket[racket] lists and  @racket[plot] them as data. Let's make use of @racket[plot] again to make views of data from faculty development questionnaires. As we work with larger amounts of data typing @racket[list]s into programs will become unweildy. So let's work with data in plain text @code{.csv} files.

@(define facfreq-eval (make-base-eval))
@(facfreq-eval '(require racket/list csv-reading plot plot/utils 
                    (for-label racket/base)))
@(define-syntax-rule (facfreq-example e ...)
  (examples #:label "Code:"  #:eval facfreq-eval e ...))
@; index.scrbl in the directory above needs symlinks to this directory's datafile
@; #:preserve-source-location must be a 'for-label' thing ...
@; I wonder if I can just symlink and entire data directory instead of each file?

@(define Q-data-file "questions.csv")

@facfreq-example[
(define Q-data-file "questions.csv")
(define (get-file-csv-data file-name)
  (define f-port (open-input-file file-name))
  (define data-lists (csv->list f-port))
  (close-input-port f-port)
  (code:comment "close-* can't be last line or prcdr returns void")
  data-lists)
(define question-data-lists
  (get-file-csv-data Q-data-file))
(code:comment "(map reverse question-data-lists)")
]
 
@(define question-data-lists
  (get-file-csv-data Q-data-file))

We see that there are eleven questions and that each questions has a short-name. The questions grouped into three types but we will not do anything with the groups for now.

@(tabular #:sep (hspace 1)
          #:column-properties '(top top top)
	  #:row-properties '(bottom-border ())
	  (cons '("n" "questions" "types" "short name")
            (number-rows (map reverse question-data-lists)))
	  )

@(define R-file "responses.csv")
@facfreq-example[
(define R-file "responses.csv")
(define response-lists
  (get-file-csv-data R-file))
(code:comment "(map reverse response-lists)")
]

@(define response-lists
  (get-file-csv-data R-file))

@(tabular #:sep (hspace 2)
          #:column-properties '(top top (center top))
	  #:row-properties '(bottom-border ())
	  (cons '("responses" "short name" "integer value")
	        (map reverse response-lists))
	  )

Students circle integers that are associated with levels of agreement with the statements that I have been calling questions so far. The five categories are ordered from one to five: one indicates least agreement and five indicates the most agreement. With this questionnaire, I think of one and two as negative responses and four and five as positive responses. Three seems neutral, a convenient response when you lack interest or are simply disintereted in the question. 

Now let's see what the frequency data for these responses to statements.

@(define frequency-data-file "TCR.csv")
@facfreq-example[
(define frequency-data-file "TCR.csv")
(define frequency-data-lists
  (get-file-csv-data frequency-data-file))
(take frequency-data-lists 6)
]

We see that the first two lines are information (meta-data?) about the questionnaire. This data is for an individual teacher's class in the second term of 2019. The number of students that filled in the questionnaire papers is forty-five. The third line of the file is liast of headers for the data that starts from the fourth line. I've seen this convention in the @code{.csv} files for classroom administration. The fourth line is where to start getting student information for generating roll sheets and seating charts. The first two lines of meta-data are useful of naming files and labeling views(plots and print-outs).

In @racket[Racket], to view the table with @racket[scribble]'s @racket[tabular] procedure all the rows have to be lists of the same length. There is no reason for the first two lines of information to be the same length as the the data.

@facfreq-example[
(define (get-info-rows data-table)
  (take data-table 2))
(define (get-header-row data-table)
  (third data-table))
(define (get-data-rows data-table)
  (drop data-table 3))
(define freq-info (get-info-rows frequency-data-lists))
(define freq-header-row (get-header-row frequency-data-lists))
(define freq-rows (get-data-rows frequency-data-lists))
freq-info
]

The meta-data information is column-based with the cells in the first row matching the cells in the second row in a @racket[dict]ionary relationship. Later we will write a @code{transpose-table} procedure to convert the rows into a dictionary form. In a later project transposing a table, the procedure to convert the column- and row-relationship, will help us to convert raw questionnaire data to the sort of frequency table we are working with in this project. The procedure allows for @racket[plot]ting a variety a views too.

@(define frequency-data-lists
  (get-file-csv-data frequency-data-file))
@(define freq-info (get-info-rows frequency-data-lists))
@(define freq-header-row (get-header-row frequency-data-lists))
@(define freq-rows (get-data-rows frequency-data-lists))

@(tabular #:sep (hspace 1)
          #:row-properties '(left right)
          freq-info)
After the first two rows of information we get the header row and then data rows.

@(tabular #:sep (hspace 1)
          #:row-properties '(bottom-border ())
	  #:column-properties '(left left right)
	  (for/list ((row-number (in-naturals))
	             (row (cons freq-header-row freq-rows)))
             (cons (number->string row-number)
	           row)))

@;facfreq-example[

@facfreq-example[
(define (transpose-table lists)
  (define (recur lst merged)
    (cond
     ((empty? (car lst)) (reverse merged))
     (else (recur (map cdr lst)
		  (cons (map car lst) merged)))))
  (recur lists '()))

(define (remove-rows-with-empty-starts table)
  (code:comment "to clean out what were empty columns from meta-data")
  (code:comment "   in the first two info-lines of data.csv files")
  (filter (λ (cell) (< 0 (string-length (car cell))))
	  table))
	  
(define freq-dict (remove-rows-with-empty-starts
                    (transpose-table freq-info)))

(require racket/string)
(string-join (map second freq-dict) ",")
]

Now we are reassured that the @code{transpose-table} and row-cleaning utilities work, and that we can use @racket[string-join] to make file names and plot labels.

@facfreq-example[
(require racket/dict)

(define (label-data dict)
  (string-join
    (for/list ((ky-vl (in-dict-pairs dict)))
      (string-join ky-vl ":"))
    ","))

(define label-base (label-data freq-dict))
label-base
]

Two of the headings or keys,  @italic{body} and @italic{members}, may not make sense now, but later we will use the same procedures to label data for groups of teachers in departments and schools. The choice of @italic{body} is influenced by C.K. Ogden's 850-word @italic{Basic English} system.

The use of @racket[racket/dict] might make it easier to write more compact and readable code.

Now let's start working the frequency rows. The data from the @code{.csv} files came into @racket[racket] through @racket[csv-reading]. All the values are @racket[string]s. We will need @racket[number]s for @racket[plot]ting but, for labels, @racket[string]s work.

@facfreq-example[
(list
 freq-header-row
 (list-ref freq-rows (sub1 9)))
]

The short-name for each question (does the responder agree with the statement or not) starts the row, then the response levels, or categories, follow in descending order. For each row the question label is at @racket[list-ref] zero which may be conveniently inuitive if we reverse the descending order of response levels to match the ascending order of @racket[list-ref] use.

@facfreq-example[
(define (rest-reverse row)
  (cons (first row)
        (reverse (rest row))))
(define freq-header-list (rest-reverse freq-header-row))

(define freq-lists (map rest-reverse freq-rows))

(list freq-header-list
      (list-ref freq-lists 0)
      (list-ref freq-lists (sub1 9)))
]

Let's get a feel for the width and height needed for one question. Taking the average of these responses does not make much sense. It the @code{R}language this sort of variable is called a @italic{factor}, and the different types of response are called @italic{levels}. Another term from @italic{statistics} is @italic{categorical variable}. Levels of agreement are just categories.

If we take the average or @italic{mean} of the responses we are assuming that agreement is like water. It is as if each student has five liters of agreement and that a reponse of @code{5}``I really think so'' is the addition of the maximum five liters, while a response of @code{1}``I do not think so at all' adds only one liter. While examining the assumptions behind the average, we might wonder if a scale of @code{0} to @code{4} might be more reasonable.

What purpose might a questionnaire like this serve?

It might help teachers to get a view of the areas with the least positive and most negative responses. So let's get a view of the positive responses in one direction and the negative responses in another.
When I do not want to respond to a question for lack of interest or some other reason, I tend to write in the most neutral response. We can make a view of the negative responses that will not tend toward the positive or negative direction. We can make the neutral line gray too for now.

Maybe there are principles for assinging colors to levels of agreement but I haven't come across them yet. I was using @code{green} for "go go go" to symbolize a good feeling towards positive responses. But the images seem better with red, gray, and black lines.

@facfreq-example[
(define sample-list (list-ref freq-lists (sub1 9)))

(define very-width 3)
(define soso-width 2)
(define what-width 1)
;; from Seven-Virus-R0-view.scrbl
(define (From0toXline x y #:width (w 1) #:color (clr 'black))
  (lines (list (vector 0 y) (vector x y)) #:width w #:color clr))
(define (FromXtoXline x1 x2 y #:width (w 1) #:color (clr 'black))
  (lines (list (vector x1 y) (vector x2 y)) #:width w #:color clr))

(define opinion-line-offset .35)
(define neutral-line-offset .6)
#; (define (yes-line 4s 5s y))

(define (plot-a-question-A qlist y)
  (define opinion-y (- y opinion-line-offset))
  (define neutral-y (- y neutral-line-offset))

  (define short-name (first qlist))
  (define 4t (list-ref qlist 4)) ;; Text string
  (define 4n (string->number 4t)) ;; Number integer
  (define 5t (list-ref qlist 5)) (define 5n (string->number 5t))
  (define 2t (list-ref qlist 2)) (define 2n (string->number 2t))
  (define 1t (list-ref qlist 1)) (define 1n (string->number 1t))
  (define 3t (list-ref qlist 3)) (define 3n (string->number 3t))
  (define 3nmid (/ 3n 2))
  
  (list (point-label (vector 0 y) short-name #:point-sym 'none)
        (FromXtoXline 4n 5n opinion-y #:width 3 #:color "red")
        (From0toXline 4n opinion-y #:color 'red)
	(From0toXline (- 2n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 2n) (- 1n) opinion-y #:width 3 #:color 'darkslategray)
	(FromXtoXline (- 3nmid) 3nmid neutral-y #:color 'gray)
   )
  )

(parameterize
  ((plot-decorations? #f)
   (plot-height 150))
  (plot
    (plot-a-question-A sample-list 9)
    #:x-min -8 
    #:y-min 8 #:y-max 10))
  
]

Let's tweak (make small changes to) this view while looking at all the questions together. We have to put the plot code inside a procedure  to set parameters with data from the question lists. We'll use lists made fromt he @code{questions.csv} file to view the entire statement along with counts of the response levels.

@facfreq-example[
(list-ref question-data-lists (sub1 9))

(list-ref freq-lists (sub1 9))
]

We can use the  @code{question-data-lists} and @code{freq-lists} as @racket[dict]ionaries that have the same keys.

@facfreq-example[
(define (get-q-text dict-name key-name)
  (last (dict-ref dict-name key-name
                  (string-append "No dict entry for" key-name))))

(define (plot-a-question-B qlist y)
  (define opinion-y (- y opinion-line-offset))
  (define neutral-y (- y neutral-line-offset))


  (define short-name (first qlist))
  (define 4t (list-ref qlist 4)) ;; Text string
  (define 4n (string->number 4t)) ;; Number integer
  (define 5t (list-ref qlist 5)) (define 5n (string->number 5t))
  (define 2t (list-ref qlist 2)) (define 2n (string->number 2t))
  (define 1t (list-ref qlist 1)) (define 1n (string->number 1t))
  (define 3t (list-ref qlist 3)) (define 3n (string->number 3t))
  (define 3nmid (/ 3n 2))   
  (define q-label (get-q-text question-data-lists short-name))
  
  
  (list (point-label (vector 0 y) q-label #:point-sym 'none)
        (FromXtoXline 4n 5n opinion-y #:width 3 #:color "red")
        (From0toXline 4n opinion-y #:color 'red)
	(From0toXline (- 2n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 2n) (- 1n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 3nmid) 3nmid neutral-y #:color 'gray)
   )
  )

(parameterize
  ((plot-decorations? #f)
   (plot-height 150))
  (plot
    (plot-a-question-B sample-list 9)
    #:x-min -8 
    #:y-min 8 #:y-max 10))
]

I want to add put the counts for each level at their point on their line later. to fit the numbers into the view we will have to tweak the @racket[plot-height]. Some of the question text might be long enough to require changes to the @racket[plot-width] too. So let's plot all the questions together and then start adjusting the width and height of the plot, and maybe the minimum (@racket[#:x-min], @racket[#:y-min])and maximum (@racket[#:x-max], @racket[#:y-max]) values for the coordinates too.

@facfreq-example[
(define (plot-a-question-C qlist y)
  (define opinion-y (- y opinion-line-offset))
  (define neutral-y (- y neutral-line-offset))


  (define short-name (first qlist))
  (define 4t (list-ref qlist 4)) ;; Text string
  (define 4n (string->number 4t)) ;; Number integer
  (define 5t (list-ref qlist 5)) (define 5n (string->number 5t))
  (define 2t (list-ref qlist 2)) (define 2n (string->number 2t))
  (define 1t (list-ref qlist 1)) (define 1n (string->number 1t))
  (define 3t (list-ref qlist 3)) (define 3n (string->number 3t))
  (define 3nmid (/ 3n 2))   
  (define q-label (get-q-text question-data-lists short-name))
  
  
  (list (point-label (vector -5 y) q-label #:point-sym 'none)
        (FromXtoXline 4n 5n opinion-y #:width 3 #:color "red")
        (From0toXline 4n opinion-y #:color 'red)
	(From0toXline (- 2n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 2n) (- 1n) opinion-y #:color 'darkslategray)
	(FromXtoXline (- 3nmid) 3nmid neutral-y #:color 'gray)
   )
  )

(define height-for-a-question 80)
(define plot-w 650)

(define (sum-string-refs lst r1 r2)
  (define s1 (list-ref lst r1))
  (define s2 (list-ref lst r2))
  (apply + (map string->number (list s1 s2))))

(define (get-opinioned-max data-lists r1 r2)
  (sum-string-refs
    (argmax (lambda (lst) (sum-string-refs lst r1 r2))
                  data-lists)
    r1 r2))	  
  
(define (plot-questions-view-A data-lists)
  (define table-length (length data-lists))
  (define plot-h (* height-for-a-question table-length))
  (define x-max (add1 (get-opinioned-max data-lists 4 5)))
  (define x-min (sub1 (- (get-opinioned-max data-lists 1 2))))
                       
  (parameterize
    ((plot-decorations? #f)
     (plot-height plot-h)
     (plot-width  plot-w))
    (plot

      (for/list ((level-frequency-list data-lists)
                 (y (in-naturals)))
        (plot-a-question-C level-frequency-list (add1 y)))
      #:x-min x-min #:x-max x-max
      #:y-min 0 #:y-max 12)))

(plot-questions-view-A freq-lists)
]

This basic form seems to have a decent @italic{ink-to-data ratio}. But the view needs a label generated from the data (@code{label-plot}). Without meaningful labels and filenames it will be hard to use the images with confidence later. The plot the lines should also be annotated with the actual figures, and maybe even the reponse type: the short name for the response.

We are repeating the use of values for levels @code{2} and @code{4} in addition to the colors for the positive and negative response lines. Reducing the repetition in defining the string and number definitions for variables like @code{4t} and @code{4n} will have to wait for a @racket[syntax-rule] or @code{macro}, but @code{plot-a-question} could be made simpler with separate procedures for @code{positives-line} and @code{negatives-line}.

@facfreq-example[
(define (label-plot info-dict x y)
  (point-label (vector x y) label-base  #:anchor 'top-right #:size 10 #:point-sym 'none))

(define x-max (add1 (get-opinioned-max freq-lists 4 5)))
(define x-min (sub1 (- (get-opinioned-max freq-lists 1 2))))
(define y-max (add1 (length freq-lists)))


(define posi-color 'red)
(define (positives-line 4n 5n y)
  (define posi-sum (+ 4n 5n))
  (list 
    (FromXtoXline 4n posi-sum y #:width 3 #:color posi-color)
    (From0toXline 4n y #:color posi-color)))

(define (get-short-label s row)
  (first (dict-ref response-lists s "No dict-ref in response-lists for short")))
(define (positives-view 4s 4n 5s 5n y)
  (define 4short "somewhat") ;;(get-short-label response-lists "4"))
  (define 5short "very") ;; (get-short-label response-lists "5"))
  (define 4label (string-append 4short ":" 4s))
  (define 5label (string-append 5short ":" 5s))
  (define posi-sum (+ 4n 5n))
  (list
    (positives-line 4n 5n y)
    (point-label (vector 4n y) 4label #:size 9 #:anchor 'top-right)
    (point-label (vector posi-sum y) 5label  #:size 9 #:anchor 'bottom-right)))

(define nega-color 'darkslategray)
(define (negatives-line 1n 2n y)
  (define nega-sum (+ 1n 2n))
  (list
    (From0toXline (- 2n) y #:color nega-color)
    (FromXtoXline (- nega-sum) (- 2n)  y #:width 3 #:color nega-color)))
(define (negatives-view x-min 1s 1n 2s 2n y)
  (define nega-sum (+ 1n 2n))
  (define 1short "not at all") ;; (get-short-label ... )
  (define 2short "not much") ;; (get-short-label ...)
  (define 1label (string-append 1short ":" 1s))
  (define 2label (string-append 2short ":" 2s))
  (list
   (negatives-line 1n 2n y)
   (point-label (vector (- 2n) y) 2label #:size 9 #:anchor 'bottom-left)
   (point-label (vector x-min y)
     1label #:size 9 #:anchor 'top-left #:point-sym 'none)))

(define ntrl-color 'gray)
(define (neutral-line 3n y)
  (define 3nmid (/ 3n 2))
  (FromXtoXline (- 3nmid) 3nmid y #:color 'gray))
(define (neutral-view s n y)
  (define 3short "neither")
  (define 3label (string-append 3short ":" s))
  (list
    (neutral-line n y)
    (point-label (vector 0 y) 3label #:size 9 #:color 'gray #:anchor 'top)))


(define (plot-a-question qlist y)
  (define opinion-y (- y opinion-line-offset))
  (define neutral-y (- y neutral-line-offset))

  (define short-name (first qlist))
  (define 4t (list-ref qlist 4)) ;; Text string
  (define 4n (string->number 4t)) ;; Number integer
  (define 5t (list-ref qlist 5)) (define 5n (string->number 5t))
  (define 2t (list-ref qlist 2)) (define 2n (string->number 2t))
  (define 1t (list-ref qlist 1)) (define 1n (string->number 1t))
  (define 3t (list-ref qlist 3)) (define 3n (string->number 3t))
  (define q-label (get-q-text question-data-lists short-name))
  
  
  (list ;; start by placiing the statement text
        (point-label (vector (add1 x-min) y) q-label #:size 12 #:point-sym 'none)
	(positives-view 4t 4n 5t 5n opinion-y)
	(negatives-view x-min 1t 1n 2t 2n opinion-y)
        (neutral-view 3t 3n neutral-y)
   ))


(define (plot-questions-view data-lists)
  (define table-length (length data-lists))
  (define plot-h (* height-for-a-question table-length))
                       
  (parameterize
    ((plot-decorations? #f)
     (plot-height plot-h)
     (plot-width  plot-w))
    (plot
      (list
        (for/list ((level-frequency-list data-lists)
                   (y (in-naturals)))
          (plot-a-question level-frequency-list (add1 y)))
	  (label-plot freq-info x-max y-max)
	  )
      #:x-min x-min #:x-max x-max
      #:y-min 0 #:y-max y-max)))

(plot-questions-view freq-lists)
]

Along with the statements' text and lines for a visual comparison of the counts for each level of response, we can also see the response counts along with a short name. The short name should help the viewer to remember the sentences associate with each level's number. It would be helpful to have the sentences for each level number right there in the view.

We will have more room in the upper right area if we move the plot label to the upper left corner. And we should re-order the statements so that questions most in need of attention are at the top of the view. After moving the label to the left, and putting at the top the questions with the least positive responses, we might have room for the responses text.

The most important reason to re-order the plot is to make it easier to make comparisons and start thinking about the results.

@facfreq-example[

(define freq-lists-sort>
  (sort freq-lists
    (lambda (l1 l2) (> (sum-string-refs l1 4 5)
                       (sum-string-refs l2 4 5)))))

(plot-questions-view freq-lists-sort>)
]

This view lets us compare student responses to a variety of statements about one class. There are a few more tweaks it that would be interesting to explore. The "not at all" responses seem important, maybe have the number for that response to the left to make it easier to attent to it's importance. Mabye the question types should be to the left of the statements. Adding the types to the view might give a quicker sense for the students attitudes towards their own participation, towards the teacher's performancs, and towards the class overall. 

Now that the view has developed this far, it might be time to think about putting the procedures into a script so we can generate plot views for several data files. With the reassurance we get from seeing @racket[examples] it may be time to write a  @racket[scribble/lp2] version that can accept file-name arguments from the command line and generate plot views for each file's data. I will leave the new placement for the @code{label-plot} as a task for the @racket[scribble/lp2] version or a simpler @racket[racket] script.

Is there a way to combine @racket[scribble/examples] with @racket[scribble/lp2]? I wish I could write a @racket[syntax-rule] that would let me generate an @code{example} where the last line outputs some sort of reassurance that the code is working, while all the other lines make up a @racket[chunk]. It would allow for documents like this one to be dual-purpose: both educational as a text/reading and functional as a script/program. 
