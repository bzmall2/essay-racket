#lang scribble/book

@title{Essay Racket}

Learning Racket Attempts.


@include-section["plot-7-virus-R0s/Seven-Virus-R0-view.scrbl"]

@; the paths to data files like questions.csv don't transfer with include-section, let's try dired Y dired-do-relsymlink to since S dired-do-symlink didn't work. absolute paths on my HD don't work on gitlab. Interesting how gitlab Pages refused to refresh this broken index.
@include-section["questionnaire-facdev-en/frequency-view.scrbl"]

@; If the relative symlinks don't work, just make an itemization list of hyperlinks for the index. 
